# [grid]
## no one needs this
There are very good css grids out there (i.e. Foundation, Twitter Bootstrap), so this is
basically just for me.
It's based on [responsivegridsystem.com][1].
And sorry for [overestimating the grids][2].
## example
    :::html
    <div class="row">
        <div class="col g1_3">
            [1 / 3]
        </div>
        <div class="void g1_3">
            invisible on mobile phone
            (used as offset class)
        </div>
        <div class="col g1_3">
            [1 / 3]
        </div>
    </div>

icon by [Visual Pharm][3]

[1]: http://www.responsivegridsystem.com/
[2]: http://css-tricks.com/dont-overthink-it-grids/
[3]: http://www.visualpharm.com